package mx.tecnm.misantla.myappexamplerecyclercard

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import mx.tecnm.misantla.myappexamplerecyclercard.adapter.PokemonAdapter
import mx.tecnm.misantla.myappexamplerecyclercard.databinding.ActivityMainBinding
import mx.tecnm.misantla.myappexamplerecyclercard.model.Pokemon

class MainActivity : AppCompatActivity() {
    val pokemons = mutableListOf<Pokemon>()
    lateinit var adaptador: PokemonAdapter
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        cargarInformacion()
        configurarAdaptador()
    }

    private fun configurarAdaptador() {
        adaptador = PokemonAdapter(pokemons)
        binding.recyclerviewPokedex.adapter = adaptador
        binding.recyclerviewPokedex.layoutManager = LinearLayoutManager(this)
    }

    private fun cargarInformacion() {

        pokemons.add(Pokemon("Bulbasaur","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"))
        pokemons.add(Pokemon("Ivysaur","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/2.png"))
        pokemons.add(Pokemon("Venusaur","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/3.png"))
        pokemons.add(Pokemon("Charmander","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/4.png"))
        pokemons.add(Pokemon("Charmelon","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/5.png"))
        pokemons.add(Pokemon("Charizard","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png"))
        pokemons.add(Pokemon("Squitle","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/7.png"))
        pokemons.add(Pokemon("Wartortle","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/8.png"))
        pokemons.add(Pokemon("Blastoise","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/9.png"))

        
    }
}