package mx.tecnm.misantla.myappexamplerecyclercard.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import mx.tecnm.misantla.myappexamplerecyclercard.R
import mx.tecnm.misantla.myappexamplerecyclercard.databinding.ActivityMainBinding
import mx.tecnm.misantla.myappexamplerecyclercard.databinding.ItemPokemonBinding
import mx.tecnm.misantla.myappexamplerecyclercard.model.Pokemon

class PokemonAdapter (var pokemons: MutableList<Pokemon>): RecyclerView.Adapter<PokemonAdapter.PokemonAdapterViewHolder>() {


    class PokemonAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        private val binding: ItemPokemonBinding = ItemPokemonBinding.bind(itemView)

        fun bind(pokemon: Pokemon){
           binding.tvNombrePokemon.text = pokemon.nombre
            Picasso.get().load(pokemon.urlImagen).error(R.drawable.ic_kotlin).into(binding.imgPokemon)

        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PokemonAdapter.PokemonAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon,parent,false)
        return  PokemonAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holder: PokemonAdapter.PokemonAdapterViewHolder, position: Int) {
       val pokemon = pokemons[position]
        holder.bind(pokemon)
    }

    override fun getItemCount(): Int {
       return  pokemons.size
    }
}